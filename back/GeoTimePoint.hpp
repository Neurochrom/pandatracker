#pragma once

#include <functional>

struct GeoTimePoint
{
   int latitudeE7;
   int longitudeE7;
   int altitudeMeters;
   uint64_t timestampSec;
   friend bool operator==(const GeoTimePoint& a, const GeoTimePoint& b)
   {
      return a.latitudeE7 == b.latitudeE7 &&
             a.longitudeE7 == b.longitudeE7 &&
             a.altitudeMeters == b.altitudeMeters &&
             a.timestampSec == b.timestampSec;
   }
};

namespace std
{

template <>
struct hash<GeoTimePoint>
{
   size_t operator()(const GeoTimePoint& k) const noexcept
   {
      return k.timestampSec ^ (size_t(k.latitudeE7) << 32)
         ^ k.longitudeE7 ^ (size_t(k.altitudeMeters) << 24);
   }
};

} // end std namespace
