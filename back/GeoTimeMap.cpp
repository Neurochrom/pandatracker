#include <future>

#include "GeoTimeMap.hpp"


std::vector<Encounter> GeoTimeMap::addEntry(const std::string& patientStrId, const GeoTimePoint& gtp, EntryType type)
{
   uint64_t patientNId = patientIds[patientStrId];
   std::vector<Encounter> encounters;

   {
      std::lock_guard<std::mutex> bouncer(histGuard[type]);
      patientHistories[type][patientNId].push_back(gtp);
   }

   auto gtaBundle = getAreas(gtp);
   {
      std::lock_guard<std::mutex> bouncer(gtaGuard[type]);
      for (GeoTimePoint& mid : gtaBundle.mids)
      {
         auto& sector = gtaMaps[type][mid];
         for (auto& d : sector.diseases)
         {
            for (auto& p : sector.patients)
            {
               Encounter e(DataPoint(mid, type), p, d);
               encounters.push_back(e);
            }
         }
         sector.patients.insert(patientNId);
      }
   }

   return encounters;
}

//std::unordered_set<uint64_t> GeoTimeMap::getEncounters(std::vector<GeoTimePoint> gtps, EntryType type) const
//{
//   std::vector<Encounter> encounters;
//
//   std::lock_guard<std::mutex> idBouncer(gtaGuard[type]);
//
//   for (auto& gtp : gtps)
//   {
//      auto gtaBundle = getAreas(gtp);
//
//      for (uint8_t i = 0; i < GTABundle::SIZE; ++i)
//      {
//         auto it = gtaMaps[type].find(gtaBundle.mids[i]);
//         if (it != gtaMaps[type].end())
//         {
//            for (auto& sickness : it->second.diseases)
//            {
//               encounters.push_back(
//            ret.insert(it->second.patients.begin(), it->second.patients.end());
//         }
//      }
//   }
//
//   return ret;
//}

std::vector<Encounter> GeoTimeMap::markSickType(uint64_t patientNId, uint32_t diseaseNId, EntryType type)
{
   std::vector<Encounter> encounters;

   std::lock_guard<std::mutex> histBouncer(histGuard[type]);
   std::lock_guard<std::mutex> bouncer(gtaGuard[type]);

   auto patHistIt = patientHistories[type].find(patientNId);
   if (patHistIt == patientHistories[type].end())
       return encounters;

   for (auto& gtp : patHistIt->second)
   {
      auto gtaBundle = getAreas(gtp);
      for (GeoTimePoint& mid : gtaBundle.mids)
      {
         auto& sector = gtaMaps[type][mid];
         sector.diseases.insert(diseaseNId);
         for (auto& p : sector.patients)
         {
            Encounter e(DataPoint(mid, type), p, diseaseNId);
            encounters.push_back(e);
         }
      }
   }
   return encounters;
}

std::vector<Encounter> GeoTimeMap::markSick(const std::string& patientStrId, const std::string& sickness)
{
   auto patientNId = patientIds[patientStrId];
   auto diseaseNId = diseaseIds[sickness];

   std::future<std::vector<Encounter>> promisedEncountersOfType[EntryType::NUM_ENTRY_TYPES];
   std::vector<Encounter> encounters;

   for (int type = 0; type < EntryType::NUM_ENTRY_TYPES; ++type)
   {
      promisedEncountersOfType[type] = std::async(&GeoTimeMap::markSickType, this,
                                                  patientNId, diseaseNId, (EntryType)type);
   }

   for (int type = 0; type < EntryType::NUM_ENTRY_TYPES; ++type)
   {
      const auto& eoft = promisedEncountersOfType[type].get();
      encounters.insert(encounters.end(), eoft.begin(), eoft.end());
   }

   return encounters;
}
