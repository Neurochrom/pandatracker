#pragma once

#include "EntryType.hpp"
#include "GeoTimePoint.hpp"

struct DataPoint : GeoTimePoint
{
   EntryType type;
   DataPoint() {}
   DataPoint(const GeoTimePoint& gtp, EntryType et) : GeoTimePoint(gtp), type(et) {}
};
