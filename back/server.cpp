#include <algorithm>
#include <iostream>
#include <vector>

#include "httplib.h"
#include "json.hpp"
#include "ston.hpp"
#include "strout.hpp"

#include "GeoTimeMap.hpp"

namespace ht = httplib;
using json = nlohmann::json;

template <class T>
void vecAppend(std::vector<T>* to, const std::vector<T>& from)
{
   to->insert(to->end(), from.begin(), from.end());
}

int main()
{
   std::cout << "Let's not die!" << std::endl;

   ht::Server server;
   GeoTimeMap gtm;

   server.Options("/(.*)", [&](const ht::Request & /*req*/, ht::Response &res)
   {
      res.set_header("Access-Control-Allow-Methods", " POST, GET, OPTIONS");
      res.set_header("Content-Type", "text/html; charset=utf-8");
      res.set_header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, Accept");
      res.set_header("Access-Control-Allow-Origin", "*");
      res.set_header("Connection", "close");
   });

   server.Get("/hi", [](const ht::Request& req, ht::Response& res)
   {
      static int counter = 0;
      res.set_header("Access-Control-Allow-Origin", "http://localhost:3000");
      res.set_content(STROUT("Hello World #" << ++counter), "text/plain");
   });

   server.Post("/locfeed", [&](const ht::Request &req, ht::Response &res, const ht::ContentReader &content_reader)
   {
      //static int counter = 0;
      //res.set_content(STROUT("Hello POST #" << ++counter), "text/plain");
      res.set_header("Access-Control-Allow-Origin", "http://localhost:3000");
      //return;

      std::string locfeedJsonStr;
      if (req.is_multipart_form_data())
      {
         ht::MultipartFormDataItems files;
         content_reader([&](const ht::MultipartFormData &file)
         {
            files.push_back(file);
            return true;
         },
                        [&](const char *data, size_t data_length)
         {
            files.back().content.append(data, data_length);
            return true;
         });

         for (auto& f : files)
            locfeedJsonStr += f.content;
      }
      else
      {
         std::string body;
            content_reader([&](const char *data, size_t data_length)
         {
               locfeedJsonStr.append(data, data_length);
            return true;
         });
      }

      std::cout << locfeedJsonStr << std::endl;
      std::vector<Encounter> encounters;

      try
      {
         std::string ret;

         const json locfeedJson = json::parse(locfeedJsonStr);

         std::string srtId = locfeedJson.at("email");

         bool isSick = locfeedJson.value("sick", false);
         std::string sickness = isSick ? "COVID-19" : "";

         auto locations = locfeedJson.find("locations");
         if (locations != locfeedJson.end())
         {
            for (auto it = locations->begin(); it != locations->end(); ++it)
            {
               GeoTimePoint gtp;
               /// @todo more error checking
               gtp.latitudeE7 = it->at("latitudeE7").get<int>();
               gtp.longitudeE7 = it->at("longitudeE7").get<int>();
               gtp.altitudeMeters = it->value("altitude", 100);   // we need just any value
               gtp.timestampSec = ston<uint64_t>(it->at("timestampMs").get<std::string>().c_str()) / 1000;
               EntryType type = EntryType::WALK; /// @todo type

               /// @todo interpolation

               vecAppend(&encounters, gtm.addEntry(srtId, gtp, type));
            }
         }

         auto data = locfeedJson.find("data");
         if (data != locfeedJson.end())
         {
         processData:
            for (auto it = data->begin(); it != data->end(); ++it)
            {
               auto visitOrMove = it->find("visitOrMove");
               if (visitOrMove != it->end() && visitOrMove->get<std::string>() == "visit")
               {
                  GeoTimePoint gtp1, gtp2;
                  {
                     auto loc = it->at("location");
                     gtp1.latitudeE7 = loc.at("latitudeE7").get<int>();
                     gtp1.longitudeE7 = loc.at("longitudeE7").get<int>();
                     gtp1.altitudeMeters = loc.value("altitude", 100);   // we need just any value
                  }
                  auto duration = it->find("duration");
                  if (duration != it->end())
                  {
                     auto st = duration->find("startTimestampMs");
                     if (st != duration->end())
                        gtp1.timestampSec = ston<uint64_t>(st->get<std::string>().c_str()) / 1000;
                     else
                        continue;
                     gtp2 = gtp1;
                     auto et = duration->find("endTimestampMs");
                     if (et != duration->end())
                        gtp2.timestampSec = ston<uint64_t>(et->get<std::string>().c_str()) / 1000;
                     else
                        continue;
                  }

                  /// @todo interpolation
                  vecAppend(&encounters, gtm.addEntry(srtId, gtp1, EntryType::INDOOR));
                  vecAppend(&encounters, gtm.addEntry(srtId, gtp2, EntryType::INDOOR));
               }
               else
               {
                  for (auto iit = it->begin(); iit != it->end(); ++iit)
                  {
                     try
                     {
                        GeoTimePoint gtp;
                        /// @todo more error checking
                        gtp.latitudeE7 = it->at("latitudeE7").get<int>();
                        gtp.longitudeE7 = it->at("longitudeE7").get<int>();
                        gtp.altitudeMeters = it->value("altitude", 100);   // we need just any value
                        gtp.timestampSec = ston<uint64_t>(it->at("timestampMs").get<std::string>().c_str()) / 1000;
                        EntryType type = EntryType::WALK; /// @todo type

                        /// @todo interpolation
                        vecAppend(&encounters, gtm.addEntry(srtId, gtp, type));
                     }
                     catch (nlohmann::detail::out_of_range& e)
                     {
                        std::cout << e.what() << " continuing\n";
                        continue;
                     }
                  }
               }
            }

            // just in case we get a malformed json with nested data;
            auto innerData = data->find("data");
            if (innerData != data->end())
            {
               data = innerData;
               goto processData;
            }

            //auto timelineObjects = data.
         }



         /// @todo lunch analysis
         if (sickness.size())
            vecAppend(&encounters, gtm.markSick(srtId, sickness));

         for (auto& e : encounters)
         {
            std::cout << "Risk encounter"
                      << " email " << gtm.getPatientStrId(e.pateintNId)
                      << " sickness " << gtm.getDiseaseStrId(e.sicknessId)
                      << " t " << e.timestampSec
                      << " long " << e.longitudeE7
                      << " lat " << e.latitudeE7
                      << " type " << toString(e.type)
                      << std::endl;
         }

         res.set_content(ret, "text/plain");
      }
      catch(std::exception& e)
      {
         std::cerr << e.what() << std::endl;
         res.set_content(e.what(), "text/plain");
      }
   });

   server.Post("/content_receiver", [&](const ht::Request &req, ht::Response &res, const ht::ContentReader &content_reader)
   {
      if (req.is_multipart_form_data())
      {
         ht::MultipartFormDataItems files;
         content_reader([&](const ht::MultipartFormData &file)
            {
               files.push_back(file);
               return true;
            },
            [&](const char *data, size_t data_length)
            {
               files.back().content.append(data, data_length);
               return true;
            });

         if (files.size())
            res.set_content(files.at(0).content, "text/plain");
      }
      else
      {
         std::string body;
         content_reader([&](const char *data, size_t data_length)
            {
               body.append(data, data_length);
               return true;
            });
         res.set_content(body, "text/plain");
      }
   });


   /// @todo require a secret token or remove
   server.Get("/stop_siuper_sicret", [&](const ht::Request& req, ht::Response& res)
   {
      server.stop();
   });

   server.listen("localhost", 1234);

   return 0;
}