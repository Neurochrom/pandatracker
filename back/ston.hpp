#pragma once

#include <type_traits>

template <class N>
N ston(const char* s)
{
   N r = 0;
   N f = (N)0.1;
   static const N minusOne = -1;

   bool neg = false;

   if (*s == '-')
   {
      neg = true;
      ++s;
   }

   if (*s == '.')
   {
      ++s;
      if (*s < '0' || *s > '9')
         throw std::runtime_error("ston failed");
      r = neg ? minusOne * f * (*s - '0') : f* (*s - '0');
      //f *= 0.1;
      f = (N)(f * 0.1);
      goto frac;
   }

   if (*s < '0' || *s > '9')
      throw std::runtime_error("ston failed");

   r = (N)(neg ? -(*s - '0') : *s - '0');
   ++s;

   for (; *s != '\0'; ++s)
   {
      if (*s < '0' || *s > '9')
      {
         if (*s == '.')
            goto frac;
         return r;
      }
      r *= 10;
      r += *s - '0';
   }

frac:
   for (++s; *s != '\0'; ++s)
   {
      if (*s < '0' || *s > '9')
         break;
      r += (*s - '0') * f;
      //f *= 0.1;
      f = (N)(f * 0.1);
   }

   return r;
}

#ifdef DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#include "doctest/doctest.h"

TEST_CASE("testing ston function")
{
   auto u32 = ston<uint32_t>("4294967295");
   CHECK(u32 == 4294967295);
   auto i64 = ston<int64_t>("9223372036854775806");
   CHECK(i64 == 9223372036854775806);
   auto d = ston<double>("01234.56789");
   CHECK(d == 1234.56789);
   auto f = ston<float>("-.2");
   CHECK(f == -0.2f);
}

#endif
