#pragma once

enum EntryType
{
   INDOOR = 0, VEHICLE, WALK, NUM_ENTRY_TYPES
};

#define ENUM_CASE(x) case x: return #x

inline const char* toString(EntryType et)
{
   switch (et)
   {
      ENUM_CASE(INDOOR);
      ENUM_CASE(VEHICLE);
      ENUM_CASE(WALK);
   }
   return "TODO throw here";
}

#undef ENUM_CASE
