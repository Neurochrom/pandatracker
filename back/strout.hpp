#pragma once

#include <sstream>

class StrOut
{
public:
   std::ostringstream oss;

   template <class T>
   StrOut& operator<<(const T& obj)
   {
      oss << obj;
      return *this;
   }

   StrOut& operator<<(std::ostream& manipulator(std::ostream&))
   {
      oss << manipulator;
      return *this;
   }
};

#define STROUT(a) (StrOut()<<a).oss.str()

#define QUOTED(a) "\""<<a<<"\""
