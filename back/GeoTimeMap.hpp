#pragma once

#include <mutex>
#include <string>
#include <unordered_map>
#include <unordered_set>

#include "EntryType.hpp"
#include "GeoTimePoint.hpp"

#include "DataPoint.hpp"

struct Encounter : DataPoint
{
   uint64_t pateintNId;
   uint32_t sicknessId;
   Encounter() {}
   Encounter(const DataPoint& dp, uint64_t pnid, uint32_t sid) :
      DataPoint(dp), pateintNId(pnid), sicknessId(sid)
   {
   }
};


template <class I>
class SyncIDtoStr
{
   std::unordered_map<I, std::string> nToStr;
   std::unordered_map<std::string, I> strToN;
   std::mutex guard;
   inline static I nextId = 0;

public:
   I operator[](const std::string& str)
   {
      std::lock_guard<std::mutex> bouncer(guard);
      auto in = strToN.insert(std::make_pair(str, nextId));
      if (in.second)
      {
         nToStr[nextId] = str;
         ++nextId;
      }
      return in.first->second;
   }

   std::string operator[](I n)
   {
      std::lock_guard<std::mutex> bouncer(guard);
      return nToStr.at(n);
   }
};

class GeoTimeMap
{
public:
   // A bundle of overlapping Geo Time Areas where a data point should be put
   struct GTABundle
   {
      static const int SIZE = 2 * 2 * 2;  // 2 for Long/Lat * 2 for alt * 2 for time
      GeoTimePoint mids[SIZE]; 
   };

   // E7 example   52 1556831
   //               0.00002 wide in latitude @ Warsaw ~= 2.2 m
   //               0.00004 wide in longitude @ Warsaw ~= 2.8 m
   //                     256 ... close enough
   inline static auto roundLatitudeE7(int latitudeE7, uint8_t geo)
   {
      return ((latitudeE7 + geo * 265) >> 8 << 8) - geo * 128;
   }
   inline static auto roundLongititudeE7(int longtitudeE7, uint8_t geo)
   {
      return ((longtitudeE7 + geo * 512) >> 9 << 9) - geo * 256;
   }

   inline static auto roundAltitudeMeters(int altitudeMeters, uint8_t alt)
   {
      return ((altitudeMeters + alt * 2) >> 1 << 1) - alt;
   }

   // 32 second windows seem just about right
   inline static auto roundTimeSec(uint64_t timestampSec, uint8_t time)
   {
      return ((timestampSec + time * 32) >> 5 << 5) - time * 16;
   }

   static GTABundle getAreas(const GeoTimePoint& gtp)
   {
      GTABundle gtas;
      for (uint8_t geo = 0; geo < 2; ++geo)
      {
         for (uint8_t alt = 0; alt < 2; ++alt)
         {
            for (uint8_t time = 0; time < 2; ++time)
            {
               gtas.mids[geo | (alt << 1) | (time << 2)].latitudeE7 = roundLatitudeE7(gtp.latitudeE7, geo);
               gtas.mids[geo | (alt << 1) | (time << 2)].longitudeE7 = roundLongititudeE7(gtp.longitudeE7, geo);
               gtas.mids[geo | (alt << 1) | (time << 2)].altitudeMeters = roundAltitudeMeters(gtp.altitudeMeters, alt);
               gtas.mids[geo | (alt << 1) | (time << 2)].timestampSec = roundTimeSec(gtp.timestampSec, time);
            }
         }
      }
      return gtas;
   }

protected:
   struct Sector
   {
      std::unordered_set<uint32_t> diseases;
      std::unordered_set<uint64_t> patients;
   };

   typedef std::unordered_map<GeoTimePoint, Sector> GTAMap;

   GTAMap gtaMaps[EntryType::NUM_ENTRY_TYPES];
   mutable std::mutex gtaGuard[EntryType::NUM_ENTRY_TYPES];

   typedef std::unordered_map<uint64_t, std::vector<GeoTimePoint>> PatientHistories;
   PatientHistories patientHistories[EntryType::NUM_ENTRY_TYPES];
   mutable std::mutex histGuard[EntryType::NUM_ENTRY_TYPES];

   SyncIDtoStr<uint64_t> patientIds;
   SyncIDtoStr<uint32_t> diseaseIds;

   typedef std::unordered_map<uint64_t, std::string> PatientToSicknessMap;
   PatientToSicknessMap whosSick;
   std::mutex sickGuard;


   std::vector<Encounter> markSickType(uint64_t patientNId, uint32_t diseaseNId, EntryType type);

public:
   std::vector<Encounter> addEntry(const std::string& patient, const GeoTimePoint& gtp, EntryType type);
   std::vector<Encounter> markSick(const std::string& strId, const std::string& sickness);

   std::string getPatientStrId(uint64_t patientNId)
   {
      return patientIds[patientNId];
   }
   std::string getDiseaseStrId(uint32_t sicknessNId)
   {
      return diseaseIds[sicknessNId];
   }

   // GTPs vector by value to avoid extensive locking - at least for now
   //std::vector<Encounter> getEncounters(std::vector<GeoTimePoint> gtps, EntryType type) const;
};
