# PandaTracker

This app analyzes location tracking history to inform about a likely exposure to a contagious disease.


# Backend build

On Linux:
```bash
cd back
make
```

On Windows use Visual Studio (tested with 2017 community edition) and the solution provided in the winBuild directory.

The built executable files should appear in the builds subdirectory.
