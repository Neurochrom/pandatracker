import React from "react";
import { Route, Switch, Router } from "react-router-dom";
import history from "./history";
import HomeScreen from "./components/HomeScreen";

import NavBar from "./components/NavBar";

import "./App.css";
const App = () => {
  return (
    <Router history={history}>
      <NavBar />
      <Switch>
        <Route path="/" exact component={HomeScreen} />
      </Switch>
    </Router>
  );
};

export default App;
