import React from "react";
import { Link, withRouter } from "react-router-dom";
import Logo from "../../assets/icons/logo.svg";
import style from "./style.module.css";

const NavBar = () => {
  return (
    <nav className={style.nav_bar}>
      <div className={style.logo_container}>
        <Link to="/">
          <img className={style.logo} src={Logo} alt="logo" />
        </Link>
        <span style={{ fontFamily: "Source Code Pro", fontWeight: 300 }}>
          <Link to="/">PandaTracker</Link>
        </span>
      </div>
    </nav>
  );
};

export default withRouter(NavBar);
