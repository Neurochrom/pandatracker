import React from "react";
import style from "./style.module.css";
import movie from "../../assets/movies/pandaTracker.m4v";
import nebulicLogo from "../../assets/icons/nebulic.svg";
import posilekDlaSenioraLogo from "../../assets/icons/posilekDlaSeniora.svg";

const HomeScreen = () => {
  return (
    <div className={style.container}>
      <div className={style.description}>
        <h1 className={style.title}>How it worked</h1>
        <p>
          Tracking people, who might have been exposed, is crucial during an
          onset of a pandemic. In contrast to other similar apps we were able to
          trace location of users from before first use of the app by using
          historical data from google takouts. In the event that you have been
          potentially exposed to a sick person, you would receive an alert about
          the threat so that you could quickly take preventive measures. Since
          Google and Apple announced their plan to create{" "}
          <strong className={style.strong}>
            <a
              href="https://www.apple.com/newsroom/2020/04/apple-and-google-partner-on-covid-19-contact-tracing-technology/"
              target="blank"
              rel="noopener noreferrer"
            >
              Covid-19 tracking app
            </a>
          </strong>
          , our mission has been completed.
        </p>
        <div className={style.about}>
          <span className={style.aboutTxt}>
            <a
              href="https://nebulic.com/"
              target="blank"
              rel="noopener noreferrer"
            >
              Powered By Nebulic
            </a>
          </span>
          <a
            href="https://nebulic.com/"
            target="blank"
            rel="noopener noreferrer"
          >
            {" "}
            <img className={style.logo} src={nebulicLogo} alt="nebulic Logo" />
          </a>
        </div>
        <div className={style.others}>
          <p>Other related charitable projects supported by Nebulic:</p>
          <a
            href="https://posilekdlaseniora.pl/"
            target="blank"
            rel="noopener noreferrer"
          >
            {" "}
            <img
              className={style.smallLogo}
              src={posilekDlaSenioraLogo}
              alt="posiłekDlaSeniora logo"
            />
          </a>
        </div>
      </div>
      <video className={style.video} playsInline loop autoPlay muted>
        <source type="video/mp4" src={movie}></source>
      </video>
    </div>
  );
};
export default HomeScreen;
